"""
Реализация тестов для свойства calculated.

В случае, если радидеталь была в статусе "рассчитана", каждое
изменение условий (например отключение проводника или изменение
напряжения в проводнике) переводит её в статус "не рассчитана".
"""


import pytest

from sample.circuit_package.connectors.wire import Wire
from sample.circuit_package.details.capasitor import Capasitor
from sample.circuit_package.details.resistor import Resistor
from sample.circuit_package.details.transistor import BipolarTransistor


@pytest.fixture(scope="function", params=[Resistor, Capasitor, BipolarTransistor])
def device(request):
    return request.param()


def test_calculated_property_if_inlet_disconnected(device):
    for direction in device.possible_input_conn_points:
        wire1 = Wire()
        wire2 = Wire()
        device.connect(wire1, direction)
        device.calculate()
        assert device.calculated is True
        device.disconnect(wire1, direction)
        device.connect(wire2, direction)
        assert device.calculated is False


def test_calculated_property_if_voltage_changed(device):
    for direction in device.possible_input_conn_points:
        wire1 = Wire()
        wire1.set_param("voltage", 10)
        device.connect(wire1, direction)
        device.calculate()
        assert device.calculated is True
        wire1.set_param("voltage", 20)
        assert device.calculated is False
