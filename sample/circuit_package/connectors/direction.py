import enum


class WireDirection(str, enum.Enum):
    """
    Константы, определяющие направление тока в проводнике.
    """

    UPSTREAM = "UPSTREAM"
    DOWNSTREAM = "DOWNSTREAM"


UPSTREAM = WireDirection.UPSTREAM
DOWNSTREAM = WireDirection.DOWNSTREAM
