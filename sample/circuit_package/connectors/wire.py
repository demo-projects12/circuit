from sample.circuit_package.circuit.circuit_object import CircuitObject
from sample.circuit_package.connectors.direction import WireDirection


class Wire(CircuitObject):
    def __init__(self, name=""):
        super().__init__(name)
        self.connections = {}
        self._current = 0
        self._voltage = 0

    def connect(self, obj, direction: WireDirection):
        self.connections[direction] = obj

    def disconnect(self, obj, direction: WireDirection):
        if direction in self.connections:
            del self.connections[direction]

    def set_param(self, param_name, value):
        params_map = {"current": self._current, "voltage": self._voltage}
        if param_name in params_map:
            params_map[param_name] = value

            # снять статус "рассчитан" у детали после этого проводника
            downstream_detail = self.connections.get(WireDirection.DOWNSTREAM)
            if downstream_detail:
                downstream_detail.reset_status()
