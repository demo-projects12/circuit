from sample.circuit_package.circuit.circuit import Circuit
from sample.circuit_package.connectors.wire import Wire
from sample.circuit_package.details.base import DetailBase
from sample.circuit_package.details.direction import DetailDirection


class CircuitFacade:
    def __init__(self, circuit: Circuit):
        self._circuit = circuit

    def add_device(self, d: DetailBase):
        _ = d

    def add_wire(self, w: Wire):
        _ = w

    def connect_wire(self, dev_name: str, wire_name: str, dd: DetailDirection):
        _ = dev_name
        _ = wire_name
        _ = dd

    def calculate_circuit(self):
        self._circuit.calculate()

    def edit_device(self, device_name: str):
        _ = device_name

    def edit_wire(self, wire_name: str):
        _ = wire_name

    def get_device_details(self, device_name: str):
        _ = device_name
