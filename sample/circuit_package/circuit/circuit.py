import networkx

from sample.circuit_package.connectors.wire import Wire
from sample.circuit_package.details.base import DetailBase


class Circuit:
    """
    Класс представляет электрическую схему.

    Поддерживаются только схемы постоянного тока.
    Электрическая схема состоит из радиодеталей и
    соединяющих их проводников.

    Attributes
    ----------
    objects: dict of {str: CircuitObject}
        Словарь, в котором содержатся ссылки на объекты схемы,
        которые могут быть радиодеталями или проводами.
    """

    def __init__(self):
        self.objects = {}
        self._td_graph = None

    @classmethod
    def from_yaml(cls, config_filename: str):
        """
        Создание схемы из её описания в формате yaml.

        Parameters
        ----------
        config_filename
            Путь к файлу с описанием схемы.

        Returns
        -------
        Circuit
        """
        circuit = cls()

        # Здесь функционал по формированию электронной схемы.

        return circuit

    @property
    def details(self) -> dict:
        """
        Вернуть список радиодеталей, включённых в схему.

        Returns
        -------
        dict of {str: DetailBase}
        """
        return {
            obj: self.objects[obj]
            for obj in self.objects
            if isinstance(self.objects[obj], DetailBase)
        }

    @property
    def wires(self) -> dict:
        """
        Вернуть список проводников, включённых в схему.

        Returns
        -------
        dict of {str: Wire}
        """
        return {
            obj: self.objects[obj]
            for obj in self.objects
            if isinstance(self.objects[obj], Wire)
        }

    def add_detail(self, detail: DetailBase):
        self.objects[detail.name] = detail
        self._initalize_graph()

    def add_wire(self, wire: Wire):
        self.objects[wire.name] = wire
        self._initalize_graph()

    def _initalize_graph(self):
        graph = networkx.MultiDiGraph()
        # Добавляем радиодетали как вершины графа
        # Добавляем проводники как рёбра графа
        self._td_graph = graph

    def __iter__(self):
        # ссылка на объект детали записана в узле графа в поле "obj_info"
        return (
            self._td_graph.nodes[node]["obj_info"]
            for node in networkx.topological_sort(self._td_graph)
        )

    def calculate(self):
        for detail in self:
            if detail.calcuted is False:
                detail.calculate()
