import enum


class CircuitObjectStatus(enum.Enum):
    UNSOLVED = 0
    SOLVED = 1


class CircuitObject:
    """
    Класс, декларирующий базовые методы всех объектов электрической схемы.
    """

    def __init__(self, name=""):
        self._status = CircuitObjectStatus.UNSOLVED
        self.name = name

    @property
    def calculated(self):
        return self._status == CircuitObjectStatus.SOLVED

    def set_status(self):
        self._status = CircuitObjectStatus.SOLVED

    def reset_status(self):
        self._status = CircuitObjectStatus.UNSOLVED

    def connect(self, obj: "CircuitObject", direction):
        """
        Присоединение других объектов схемы.
        """

    def disconnect(self, obj: "CircuitObject", direction):
        """
        Отсоединение других объектов схемы.
        """

    def calculate(self):
        raise NotImplementedError()
