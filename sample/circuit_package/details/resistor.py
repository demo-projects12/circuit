from sample.circuit_package.details.base import DetailBase


class Resistor(DetailBase):
    def __init__(self, name=""):
        super().__init__(name)

    @DetailBase.set_solved_status
    def calculate(self):
        print("Calculate a Resistor")
