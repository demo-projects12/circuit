from sample.circuit_package.details.base import DetailBase
from sample.circuit_package.details.direction import DetailInputDir, DetailOutputDir


class BipolarTransistor(DetailBase):
    def __init__(self, name=""):
        super().__init__(name)
        self._possible_conn_points = {
            DetailInputDir.BASE,
            DetailInputDir.COLLECTOR,
            DetailOutputDir.EMITTER,
        }

    @DetailBase.set_solved_status
    def calculate(self):
        print("Calculate a BipolarTransistor")
