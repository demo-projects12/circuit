from sample.circuit_package.circuit.circuit_object import CircuitObject
from sample.circuit_package.connectors.direction import (
    DOWNSTREAM,
    UPSTREAM,
    WireDirection,
)
from sample.circuit_package.connectors.wire import Wire
from sample.circuit_package.details.direction import (
    DetailDirection,
    DetailInputDir,
    DetailOutputDir,
)


class DetailBase(CircuitObject):
    """
    Определение базового класса радиодетали.
    """

    def __init__(self, name=""):
        super().__init__(name)
        self.connect_points = {}
        self._possible_conn_points = {DetailInputDir.IN, DetailOutputDir.OUT}

    @property
    def possible_input_conn_points(self):
        return [d for d in self._possible_conn_points if d in DetailInputDir]

    # TODO: уместен ли здесь этот декоратор?
    def set_solved_status(calculate_method):
        """
        Декоратор для метода calculate(), задающий статус calculated.

        Перед расчётом состояние calculated сбраывается в UNSOLVED,
        после расчета выставлятеся в SOLVED.
        Применимо к некоторым аппаратам.
        """

        def _wrapper(self):
            self.reset_status()
            calculate_method(self)
            self.set_status()

        return _wrapper

    def connect(self, wire: Wire, direction: DetailDirection):
        """
        Подсоединение проводника.
        Parameters
        ----------
        wire
        direction

        Returns
        -------

        """
        self.connect_points[direction] = wire

        # создаём также подключение у подсоединямого проводника
        # чтобы он "знал" о подключенной радиодетали
        wire_dir = get_wire_dir(direction)
        wire.connect(self, wire_dir)

        # сбросить статус "рассчитано"
        self.reset_status()

    def disconnect(self, wire: Wire, direction: DetailDirection):
        if direction in self.connect_points:
            del self.connect_points[direction]

        # отсоединяем также эту радиодеталь от подключенного проводника
        # чтобы он "забыл" о подключенной радиодетали
        wire_dir = get_wire_dir(direction)
        wire.disconnect(self, wire_dir)

        # сбросить статус "рассчитано"
        self.reset_status()

    def get_connected_wire_direction(self, wire_name) -> DetailDirection:
        """
        Получить направление, к которому подключен объект на схеме
        """
        conn_direct = [
            direct
            for direct in self.connect_points
            if self.connect_points[direct].name == wire_name
        ]
        if conn_direct:
            return conn_direct[0]
        return None


def get_wire_dir(direction: DetailDirection) -> WireDirection:
    """
    Вернуть направление проводника, соответствующее направлению детали.

    Parameters
    ----------
    direction: DetailDirection
        Направление в точке подсоеднинея радиодетали, к которому подключен поток.

    Returns
    -------
    WireDirection
        Направление тока в проводнике.
    """
    return DOWNSTREAM if direction in DetailInputDir else UPSTREAM
