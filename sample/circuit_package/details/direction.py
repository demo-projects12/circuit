import enum


class DetailDirection(str, enum.Enum):
    """
    Константы, определяющие направление тока в проводнике.
    """


class DetailInputDir(DetailDirection):
    """
    Константы, определяющие направление тока в проводнике.
    """

    IN = "IN"
    COLLECTOR = "COLLECTOR"
    BASE = "BASE"


class DetailOutputDir(DetailDirection):
    """
    Константы, определяющие направление тока в проводнике.
    """

    OUT = "OUT"
    EMITTER = "EMITTER"
