# -*- coding: utf-8 -*-

from setuptools import find_packages, setup

with open("README.md", encoding="utf-8") as f:
    readme = f.read()

with open("requirements.txt") as fp:
    install_requires = fp.read().splitlines()

setup(
    name="circuit_package",
    version="0.1.0",
    description="Basic circuit_package project",
    long_description=readme,
    author="Eugene Bogdanov",
    author_email="eugenybogdanov@inbox.ru",
    url="https://gitlab.com/demo-projects12/circuit",
    packages=find_packages(exclude=("tests", "docs")),
    install_requires=install_requires,
)
