from invoke import task

DIRNAME = "sample"
PROJECT = "sample"


@task
def autoflake(c):
    command = "autoflake"
    options = [
        "--in-place",
        "--recursive",
        "--remove-all-unused-imports",
        "--ignore-init-module-imports",
    ]

    print("autoflake is running...")
    c.run(
        "{command} {options} {dirname}".format(
            command=command, options=" ".join(options), dirname=DIRNAME
        )
    )


@task
def isort(c):
    print("isort is running...")
    c.run("isort --project {} {}".format(DIRNAME, PROJECT))


@task
def black(c):
    print("black is running...")
    c.run("black {}".format(DIRNAME))


@task
def lint(c):
    black(c)
    isort(c)
    autoflake(c)
